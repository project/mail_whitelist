Mail Whitelist
==============

This module allows you to manage a whitelist of e-mail addresses that may receive
mails from your website, which can be quite handy when you copy a live database to
a test or development environment...

Installation
------------

Just install the module as normal, there are no dependencies!

Configuration
-------------

Browse to /admin/config/system/mail-whitelist to change the default settings.
All settings are properly documented in the form itself, but don't hesitate
to open an issue op drupal.org if something isn't quite clear.
